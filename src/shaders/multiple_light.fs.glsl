#version 330

in vec3 vViewSpaceNormal;
in vec2 vTexCoords;
in vec3 vViewSpacePosition;
in mat3 vTBN;

struct DirectionalLight
{
    vec3 direction;
    vec3 intensity;
};

uniform DirectionalLight uDLight;
uniform int uDLightEnable;

struct PointLight
{
    vec3 position;
    vec3 intensity;
    float constant;
    float linear;
    float quadratic;
};

uniform PointLight uPLight;
uniform int uPLightEnable;

struct SpotLight
{
    vec3 position;
    vec3 direction;
    vec3 intensity;
    float cutOff;
    float outerCutOff;
};

uniform SpotLight uSLight;
uniform int uSLightEnable;

uniform SpotLight uCamLight;
uniform int uCamLightEnable;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOcclusionTexture;
uniform sampler2D uNormalTexture;

uniform vec4  uBaseColorFactor;
uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform vec3  uEmissiveFactor;
uniform float uOcclusionFactor;

uniform int uOcclusionEnable;
uniform int uNormalTextureEnable;


out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

vec3 LINEARtoSRGB(vec3 color)  { return pow(color, vec3(INV_GAMMA)); }
vec4 SRGBtoLINEAR(vec4 srgbIn) { return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w); }

#define DIRECTIONAL 1
#define POINT 2
#define SPOT 3
#define CAMERA 4

vec3 computeLights(vec3 N, int lType, float theta)
{
    vec3 L;
    switch (lType) 
    {
        case DIRECTIONAL : L = uDLight.direction; break;
        case POINT : L = normalize(uPLight.position - vViewSpacePosition); break;
        case SPOT :  L = normalize(uSLight.position - vViewSpacePosition); break;
        default : return vec3(0., 0., 0.);
    }
    vec3 V = normalize(-vViewSpacePosition);
    vec3 H = normalize(L + V);

    vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 metallicRoughnessFromTexture = texture(uMetallicRoughnessTexture, vTexCoords);

    vec4 baseColor = uBaseColorFactor * baseColorFromTexture;

    vec3 metallic = vec3(uMetallicFactor * metallicRoughnessFromTexture.b);
    float roughness = uRoughnessFactor * metallicRoughnessFromTexture.g;

    vec3 dielectricSpecular = vec3(0.04);
    vec3 black = vec3(0.);

    vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic);
    vec3 F_0 = mix(vec3(dielectricSpecular), baseColor.rgb, metallic);
    float alpha = roughness * roughness;

    float VdotH = clamp(dot(V, H), 0., 1.);
    float baseShlickFactor = 1 - VdotH;
    float shlickFactor = baseShlickFactor * baseShlickFactor;
    shlickFactor *= shlickFactor;
    shlickFactor *= baseShlickFactor;
    vec3 F = F_0 + (vec3(1) - F_0) * shlickFactor;

    float sqrAlpha = alpha * alpha;

    float NdotL = clamp(dot(N, L), 0., 1.);
    
    float NdotV = clamp(dot(N, V), 0., 1.);
    float visDenominator =
        NdotL * sqrt(NdotV * NdotV * (1 - sqrAlpha) + sqrAlpha) +
        NdotV * sqrt(NdotL * NdotL * (1 - sqrAlpha) + sqrAlpha);
    float Vis = visDenominator > 0. ? 0.5 / visDenominator : 0.0;

    float NdotH = clamp(dot(N, H), 0., 1.);
    float baseDenomD = (NdotH * NdotH * (sqrAlpha - 1.) + 1.0);
    float D = M_1_PI * sqrAlpha / (baseDenomD * baseDenomD);

    vec3 f_specular = F * Vis * D;

    vec3 diffuse = c_diff * M_1_PI;

    vec3 f_diffuse = (1. - F) * diffuse;

    vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

    vec3 color = vec3(0., 0., 0.);
    switch (lType) 
    {
        case DIRECTIONAL : 
            color = (f_diffuse + f_specular) * uDLight.intensity * NdotL; 
            break;
        case POINT : 
            float lDistance   = length(uPLight.position - vViewSpacePosition);
            float attenuation = 1.0 / (uPLight.constant + uPLight.linear * lDistance + uPLight.quadratic * (lDistance * lDistance)); 
            color = (f_diffuse + f_specular) * uPLight.intensity * attenuation * NdotL; 
            break;
        case SPOT : 
            float epsilon   = uSLight.cutOff - uSLight.outerCutOff;
            float intensity = clamp((theta - uSLight.outerCutOff) / epsilon, 0.0, 1.0);
            color = (f_diffuse + f_specular) * intensity * NdotL; 
            break;
        default : return vec3(0., 0., 0.);
    }

    color += emissive;

    if (uOcclusionEnable == 1)
    {
        float occlusionFromTexture = texture2D(uOcclusionTexture, vTexCoords).r;
        color = mix(color, color * occlusionFromTexture, uOcclusionFactor);
    }

    return LINEARtoSRGB(color);
}


vec3 detectSpotLights(vec3 N, int lType)
{
    vec3 L;
    float theta;
    float outerCutOff;
    if (lType == SPOT)
    {
        L = normalize(uSLight.position - vViewSpacePosition);
        theta = dot(L, normalize(-uSLight.direction));
        outerCutOff = uSLight.outerCutOff;
    }
    if (lType == CAMERA)
    {
        L = normalize(uCamLight.position - vViewSpacePosition);
        theta = dot(L, normalize(-uCamLight.direction));
        outerCutOff = uCamLight.outerCutOff;
    }

    if(theta > outerCutOff) 
    {  
        return computeLights(N, SPOT, theta);
    } else {
        return vec3(0., 0., 0.);
    }
}



void main()
{
  vec3 N = vViewSpaceNormal;
  if (uNormalTextureEnable == 1)
  {
      N = texture(uNormalTexture, vTexCoords).rgb;
      N = N * 2.0 - 1.0;
      N = vTBN * N;
  }
  N = normalize(N);

  vec3 lColor = vec3(0., 0., 0.);
  if (uDLightEnable == 1) lColor += computeLights(N, DIRECTIONAL, 0.);
  if (uPLightEnable == 1) lColor += computeLights(N, POINT, 0.);
  if (uSLightEnable == 1) lColor += detectSpotLights(N, SPOT);
  if (uCamLightEnable == 1) lColor += detectSpotLights(N, CAMERA);
  
  fColor = lColor;
}