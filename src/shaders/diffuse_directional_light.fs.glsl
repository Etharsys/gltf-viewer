#version 330

#define PI 3.14159265359

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

out vec3 fColor;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

void main()
{
    vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
    fColor = 1 / PI * uLightIntensity * dot(viewSpaceNormal, uLightDirection);
}