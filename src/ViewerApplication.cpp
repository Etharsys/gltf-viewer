#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto      modelViewProjMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto          modelViewMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto             normalMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto          dLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uDLight.direction");
  const auto          dLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uDLight.intensity");
  const auto             dLightEnableLocation = glGetUniformLocation(glslProgram.glId(), "uDLightEnable");

  const auto           pLightPositionLocation = glGetUniformLocation(glslProgram.glId(), "uPLight.position");
  const auto          pLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uPLight.intensity");
  const auto           pLightConstantLocation = glGetUniformLocation(glslProgram.glId(), "uPLight.constant");
  const auto             pLightLinearLocation = glGetUniformLocation(glslProgram.glId(), "uPLight.linear");
  const auto          pLightQuadraticLocation = glGetUniformLocation(glslProgram.glId(), "uPLight.quadratic");
  const auto             pLightEnableLocation = glGetUniformLocation(glslProgram.glId(), "uPLightEnable");

  const auto           sLightPositionLocation = glGetUniformLocation(glslProgram.glId(), "uSLight.position");
  const auto          sLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uSLight.direction");
  const auto          sLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uSLight.intensity");
  const auto             sLightCutOffLocation = glGetUniformLocation(glslProgram.glId(), "uSLight.cutOff");
  const auto        sLightOuterCutOffLocation = glGetUniformLocation(glslProgram.glId(), "uSLight.outerCutOff");
  const auto             sLightEnableLocation = glGetUniformLocation(glslProgram.glId(), "uSLightEnable");

  const auto         camLightPositionLocation = glGetUniformLocation(glslProgram.glId(), "uCamLight.position");
  const auto        camLightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uCamLight.direction");
  const auto        camLightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uCamLight.intensity");
  const auto           camLightCutOffLocation = glGetUniformLocation(glslProgram.glId(), "uCamLight.cutOff");
  const auto      camLightOuterCutOffLocation = glGetUniformLocation(glslProgram.glId(), "uCamLight.outerCutOff");
  const auto           camLightEnableLocation = glGetUniformLocation(glslProgram.glId(), "uCamLightEnable");

  const auto         baseColorTextureLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto metallicRoughnessTextureLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto          emissiveTextureLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto         occlusionTextureLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto            normalTextureLocation = glGetUniformLocation(glslProgram.glId(), "uNormalTexture");

  const auto          baseColorFactorLocation = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto           metallicFactorLocation = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto          roughnessFactorLocation = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto           emissiveFactorLocation = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto          occlusionFactorLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionFactor");
  const auto          occlusionEnableLocation = glGetUniformLocation(glslProgram.glId(), "uOcclusionEnable");
  const auto      normalTextureEnableLocation = glGetUniformLocation(glslProgram.glId(), "uNormalTextureEnable");


  tinygltf::Model model;
  loadGltfFile(model);

  // choice from the GUI
  glm::vec3 bboxMin {};
  glm::vec3 bboxMax {};
  computeSceneBounds(model, bboxMin, bboxMax);
  glm::vec3 center = (bboxMax + bboxMin); center /= 2.;
  glm::vec3 eye = bboxMax - bboxMin + center;
  glm::vec3 up {0, 1, 0};  
  if (bboxMax.z == bboxMin.z) 
  {
    eye = center + 2.f * glm::cross(eye, up);
  }

  // Build projection matrix
  auto maxDistance = glm::length(eye);
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  if (m_hasUserCamera) {
    selectCameraMode(maxDistance, m_userCamera);

  } else {    
    //cameraController.setCamera(Camera{glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)});
    selectCameraMode(maxDistance, Camera{eye, center, up});
  }

  // Build textures
  const auto textureObjects = createTextureObjects(model);
  
  GLuint whiteTexture = 0;
  float white[] = { 1, 0, 0, 1 };
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);



  // Build VAOs
  auto bufferObjects = createBufferObjects(model);

  std::vector<VaoRange> meshIndexToVaoRange;
  auto vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // set up textures
  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if (materialIndex >= 0)
    {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      if (baseColorFactorLocation >= 0) {
        glUniform4f(baseColorFactorLocation,
          (float)pbrMetallicRoughness.baseColorFactor[0],
          (float)pbrMetallicRoughness.baseColorFactor[1],
          (float)pbrMetallicRoughness.baseColorFactor[2],
          (float)pbrMetallicRoughness.baseColorFactor[3]);
      }
      if (metallicFactorLocation >= 0) {
        glUniform1f(metallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(roughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (emissiveFactorLocation >= 0) {
        glUniform3f(emissiveFactorLocation, (float)material.emissiveFactor[0],
                                            (float)material.emissiveFactor[1],
                                            (float)material.emissiveFactor[2]);
      }
      if (occlusionFactorLocation >= 0) {
        glUniform1f(occlusionFactorLocation, (float)material.occlusionTexture.strength);
      }
      if (baseColorTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(baseColorTextureLocation, 0);
      }
      if (metallicRoughnessTextureLocation >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(metallicRoughnessTextureLocation, 1);
      }
      if (emissiveTextureLocation >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture =
              model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(emissiveTextureLocation, 2);
      }
      if (occlusionTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(occlusionTextureLocation, 3);
      }
      if (normalTextureLocation >= 0) {
        auto textureObject = whiteTexture;
        if (material.normalTexture.index >= 0) {
          const auto &texture = model.textures[material.normalTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(normalTextureLocation, 4);
      }

    } else {
      if (baseColorFactorLocation >= 0) {
        glUniform4f(baseColorFactorLocation, 1, 1, 1, 1);
      }
      if (metallicFactorLocation >= 0) {
        glUniform1f(metallicFactorLocation, 1.f);
      }
      if (roughnessFactorLocation >= 0) {
        glUniform1f(roughnessFactorLocation, 1.f);
      }
      if (emissiveFactorLocation >= 0) {
        glUniform3f(roughnessFactorLocation, 0.f, 0.f, 0.f);
      }
      if (occlusionFactorLocation >= 0) {
        glUniform1f(occlusionFactorLocation, 0.f);
      }
      if (baseColorTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(baseColorTextureLocation, 0);
      }
      if (metallicRoughnessTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(metallicRoughnessTextureLocation, 1);
      }
      if (emissiveTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(emissiveTextureLocation, 2);
      }
      if (occlusionTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(occlusionTextureLocation, 3);
      }
      if (normalTextureLocation >= 0) {
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(normalTextureLocation, 4);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          glm::mat4 modelMatrix;
          const auto &node = model.nodes[nodeIdx];
          modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          if (node.mesh >= 0)
          {
            const auto MVMatrix  = viewMatrix * modelMatrix;
            const auto MVPMatrix = projMatrix * MVMatrix;
            glUniformMatrix4fv(modelViewMatrixLocation    , 1, GL_FALSE, glm::value_ptr(MVMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(projMatrix * MVMatrix));
            glUniformMatrix4fv(normalMatrixLocation       , 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(MVMatrix))));

            glUniform1i(occlusionEnableLocation, occlusion);
            glUniform1i(normalTextureEnableLocation, normalTexture);

            glUniform1i(dLightEnableLocation, dLightEnable);
            if (dLightEnable) {
              glUniform3fv(dLightDirectionLocation, 1, glm::value_ptr(glm::normalize(glm::vec3(viewMatrix * dLight.direction))));
              glUniform3fv(dLightIntensityLocation, 1, glm::value_ptr(dLight.intensity));
            }

            glUniform1i (pLightEnableLocation, pLightEnable);
            if (pLightEnable) {
              glUniform3fv(pLightPositionLocation , 1, glm::value_ptr(glm::vec3(viewMatrix * pLight.position)));
              glUniform3fv(pLightIntensityLocation, 1, glm::value_ptr(glm::vec3(pLight.intensity)));
              glUniform1f (pLightConstantLocation , pLight.constant);
              glUniform1f (pLightLinearLocation   , pLight.linear);
              glUniform1f (pLightQuadraticLocation, pLight.quadratic);
            }

            glUniform1i(sLightEnableLocation, sLightEnable);
            if (sLightEnable) {
              glUniform3fv(sLightPositionLocation , 1, glm::value_ptr(glm::vec3(viewMatrix * sLight.position)));
              glUniform3fv(sLightDirectionLocation, 1, glm::value_ptr(glm::normalize(glm::vec3(viewMatrix * sLight.direction))));
              glUniform3fv(sLightIntensityLocation, 1, glm::value_ptr(glm::vec3(sLight.intensity)));
              glUniform1f (sLightCutOffLocation      , glm::cos(glm::radians(sLight.cutOff)));
              glUniform1f (sLightOuterCutOffLocation , glm::cos(glm::radians(sLight.outerCutOff)));
            }

            glUniform1i(camLightEnableLocation, cameraLighting);
            if (cameraLighting) {
              glUniform3fv(camLightPositionLocation , 1, glm::value_ptr(camera.eye()));
              glUniform3fv(camLightDirectionLocation, 1, glm::value_ptr(glm::normalize(camera.front())));
              glUniform3fv(camLightIntensityLocation, 1, glm::value_ptr(glm::vec3(sLight.intensity)));
              glUniform1f (camLightCutOffLocation      , glm::cos(glm::radians(sLight.cutOff)));
              glUniform1f (camLightOuterCutOffLocation , glm::cos(glm::radians(sLight.outerCutOff)));
            }

            const auto& mesh = model.meshes[node.mesh];
            const auto& vaoRange = meshIndexToVaoRange[node.mesh];

            for (int primIdx = 0; primIdx < mesh.primitives.size(); ++primIdx)
            {
              const auto vao = vertexArrayObjects[vaoRange.begin + primIdx];
              glBindVertexArray(vao);

              const auto primitive = mesh.primitives[primIdx];

              bindMaterial (primitive.material);

              if (primitive.indices >= 0)
              {
                const auto& accessor = model.accessors[primitive.indices];
                const auto& bufferView = model.bufferViews[accessor.bufferView];
                const auto& byteOffset = bufferView.byteOffset + accessor.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const void*) byteOffset);
              } else 
              {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }

          for (const auto childNodeIdx : node.children)
          {
            drawNode(childNodeIdx, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for (int nodeIdx = 0; nodeIdx < model.scenes[model.defaultScene].nodes.size(); ++nodeIdx)
      {
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  // command line to output image : build/bin/gltf-viewer viewer gltf-sample-models/2.0/DamagedHelmet/glTF/DamagedHelmet.gltf --output test.png
  if (!m_OutputPath.empty())
  {
    std::vector<unsigned char> pixels (m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() { drawScene(cameraController->getCamera()); });
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, &pixels[0]);
    stbi_write_png(m_OutputPath.string().c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        if (ImGui::Checkbox("Enable FPS camera", &cameraType)) {
          selectCameraMode(maxDistance, camera);
        }
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
      }
      if (ImGui::CollapsingHeader("Lights", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Checkbox("Enable occlusions", &occlusion);
        ImGui::Checkbox("Enable normal texture", &normalTexture);
        ImGui::Checkbox("Enable Camera light", &cameraLighting);
        ImGui::Checkbox("Enable Point Light", &pLightEnable);
        ImGui::Checkbox("Enable Directional Light", &dLightEnable);
        ImGui::Checkbox("Enable Spot Light", &sLightEnable);
        if (ImGui::SliderFloat("DirectionalLight_01 horizontal", &phi, 0, glm::pi<float>()) ||
            ImGui::SliderFloat("DirectionalLight_01 vertical", &theta, 0, glm::pi<float>())) {
          dLight.direction = glm::vec4(sin(theta)*cos(phi), cos(theta), sin(theta)*cos(phi), 0.f);
        } else if (cameraLighting) {
          dLight.direction = glm::vec4(0.f, 0.f, 1.f, 0.f);
        }
        ImGui::ColorEdit3("DirectionalLight_01 intensity", (float *) &(dLight.intensity));
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}


void ViewerApplication::selectCameraMode(float maxDistance, const Camera& camera)
{
  if (cameraType == 0) {
    cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 225.f/100 * maxDistance);
  } else {
    cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 25.f/100 * maxDistance);
  }
  cameraController->setCamera(camera);
}


bool ViewerApplication::loadGltfFile(tinygltf::Model & model)
{
  using namespace tinygltf;

  TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return -1;
  }
  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects( const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects ( model.buffers.size() );

  glGenBuffers(model.buffers.size(), bufferObjects.data());

  for (int i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);


  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, 
                      const std::vector<GLuint> &bufferObjects, 
                      std::vector<VaoRange> & meshIndexToVaoRange)
{
  std::vector<GLuint> vertexArrayObjects;

  meshIndexToVaoRange.resize(model.meshes.size());

  const GLuint VERTEX_ATTRIB_POSITION_IDX  = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX    = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;
  const GLuint VERTEX_ATTRIB_TANGENT_IDX   = 3;

  for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx)
  {
    const auto &mesh = model.meshes[meshIdx];
    const auto vaoOffset = vertexArrayObjects.size();

    meshIndexToVaoRange[meshIdx].begin = GLsizei(vertexArrayObjects.size()); 
    meshIndexToVaoRange[meshIdx].count = GLsizei(vaoOffset); 
    vertexArrayObjects.resize(vaoOffset + mesh.primitives.size());

    glGenVertexArrays(mesh.primitives.size(), &vertexArrayObjects[vaoOffset]);

    for (int primitiveIdx = 0; primitiveIdx < mesh.primitives.size(); ++primitiveIdx)
    {
      const auto &primitive = mesh.primitives[primitiveIdx];
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);

      for (int attr = 0; attr < 4; ++attr)
      {
        std::string attrName {};
        GLuint attrVertex {};
        switch (attr)
        {
          case 0 :  attrName = "POSITION";   attrVertex = VERTEX_ATTRIB_POSITION_IDX;  break;
          case 1 :  attrName = "NORMAL";     attrVertex = VERTEX_ATTRIB_NORMAL_IDX;    break;
          case 2 :  attrName = "TEXCOORD_0"; attrVertex = VERTEX_ATTRIB_TEXCOORD0_IDX; break;
          case 3 :  attrName = "TANGENT";    attrVertex = VERTEX_ATTRIB_TANGENT_IDX;   break;
          default : break;
        }

        const auto iterator = primitive.attributes.find(attrName);
        if (iterator != end(primitive.attributes)) 
        {
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx];
          const auto &bufferView = model.bufferViews[accessor.bufferView];
          const auto bufferIdx = bufferView.buffer;

          glEnableVertexAttribArray(attrVertex);
          assert(GL_ARRAY_BUFFER == bufferView.target);
          
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto offset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(attrVertex, accessor.type,
              accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
              (const GLvoid *)offset);
            
          glBindBuffer(GL_ARRAY_BUFFER, 0);
        } else {
          if (attrName == "TANGENT")
          {
            std::cout << "Generating tangents ... " << std::endl;
            std::vector<glm::vec3> tangents = computeTangents(model, primitive);
            glEnableVertexAttribArray(attrVertex);

            GLuint vboTangent;
            glGenBuffers(1, &vboTangent);
            glBindBuffer(GL_ARRAY_BUFFER, vboTangent);
              glBufferStorage(GL_ARRAY_BUFFER, tangents.size() * 3 * sizeof(GL_FLOAT), tangents.data(), 0);
            
            glVertexAttribPointer(attrVertex, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
          } 
        }
      }
      
      if (primitive.indices >= 0)
      {
        const auto accessorIdx = primitive.indices;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }

    }
  }
  glBindVertexArray(0);

  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const
{
  std::vector<GLuint> textureObjects ( model.textures.size(), 0 );

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapR = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapS = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (int i = 0; i < model.textures.size(); ++i) 
  {
    const auto &texture = model.textures[i];
    assert(texture.source >= 0);
    const auto &image = model.images[texture.source];

    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);


  return textureObjects;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
