#pragma once

#include "utils/GLFWHandle.hpp"
#include "utils/cameras.hpp"
#include "utils/filesystem.hpp"
#include "utils/shaders.hpp"
#include "utils/lights.hpp"
#include <tiny_gltf.h>



class ViewerApplication
{
public:
  ViewerApplication(const fs::path &appPath, uint32_t width, uint32_t height,
      const fs::path &gltfFile, const std::vector<float> &lookatArgs,
      const std::string &vertexShader, const std::string &fragmentShader,
      const fs::path &output);

  int run();

private:
  // A range of indices in a vector containing Vertex Array Objects
  struct VaoRange
  {
    GLsizei begin; // Index of first element in vertexArrayObjects
    GLsizei count; // Number of elements in range
  };

  GLsizei m_nWindowWidth = 1280;
  GLsizei m_nWindowHeight = 720;

  const fs::path m_AppPath;
  const std::string m_AppName;
  const fs::path m_ShadersRootPath;

  fs::path m_gltfFilePath;
  std::string m_vertexShader = "forward.vs.glsl";
  // NORMALS shader
  // std::string m_fragmentShader = "normals.fs.glsl";
  // LIGHT shader
  // std::string m_fragmentShader = "diffuse_directional_light.fs.glsl";
  // TEXTURE shader
  std::string m_fragmentShader = "multiple_light.fs.glsl";

  bool m_hasUserCamera = false;
  bool cameraType = 1;
  std::unique_ptr<CameraController> cameraController;
  Camera m_userCamera;

  bool occlusion = true;
  bool normalTexture = true;

  DirectionalLight dLight;
  float phi   = 0.;
  float theta = 0.;
  bool dLightEnable = false;

  PointLight pLight { glm::vec3(0., 1., 0.) };
  bool pLightEnable = false;

  SpotLight sLight { glm::vec3(0., 1., 0.), glm::vec3(-1., 0., 0.) };
  bool sLightEnable = false;

  SpotLight cLight;
  bool cameraLighting = true;

  fs::path m_OutputPath;

  // Order is important here, see comment below
  const std::string m_ImGuiIniFilename;
  // Last to be initialized, first to be destroyed:
  GLFWHandle m_GLFWHandle{int(m_nWindowWidth), int(m_nWindowHeight),
      "glTF Viewer",
      m_OutputPath.empty()}; // show the window only if m_OutputPath is empty
  /*
    ! THE ORDER OF DECLARATION OF MEMBER VARIABLES IS IMPORTANT !
    - m_ImGuiIniFilename.c_str() will be used by ImGUI in ImGui::Shutdown, which
    will be called in destructor of m_GLFWHandle. So we must declare
    m_ImGuiIniFilename before m_GLFWHandle so that m_ImGuiIniFilename
    destructor is called after.
    - m_GLFWHandle must be declared before the creation of any object managing
    OpenGL resources (e.g. GLProgram, GLShader) because it is responsible for
    the creation of a GLFW windows and thus a GL context which must exists
    before most of OpenGL function calls.
  */

  bool loadGltfFile(tinygltf::Model & model);

  std::vector<GLuint> createVertexArrayObjects( const tinygltf::Model &model, 
                        const std::vector<GLuint> &bufferObjects, 
                        std::vector<VaoRange> & meshIndexToVaoRange);

  std::vector<GLuint> createBufferObjects( const tinygltf::Model &model);

  void selectCameraMode(float maxDistance, const Camera& camera);

  std::vector<GLuint> createTextureObjects(const tinygltf::Model &model) const;
};