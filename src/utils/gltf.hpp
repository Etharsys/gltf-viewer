#pragma once

#include <glm/glm.hpp>
#include <tiny_gltf.h>

#include <vector>

glm::mat4 getLocalToWorldMatrix(
    const tinygltf::Node &node, const glm::mat4 &parentMatrix);

void computeSceneBounds(
    const tinygltf::Model &model, glm::vec3 &bboxMin, glm::vec3 &bboxMax);

std::vector<glm::vec3> computeTangents(
    const tinygltf::Model &model, const tinygltf::Primitive &primitive);

glm::vec3 getTangent(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec3& pos3, 
                     const glm::vec2& uv1 , const glm::vec2& uv2 , const glm::vec2& uv3);