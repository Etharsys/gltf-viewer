#include "gltf.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include <iostream>

glm::mat4 getLocalToWorldMatrix(
    const tinygltf::Node &node, const glm::mat4 &parentMatrix)
{
  // Extract model matrix
  // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#transformations
  if (!node.matrix.empty()) {
    return parentMatrix * glm::mat4(node.matrix[0], node.matrix[1],
                              node.matrix[2], node.matrix[3], node.matrix[4],
                              node.matrix[5], node.matrix[6], node.matrix[7],
                              node.matrix[8], node.matrix[9], node.matrix[10],
                              node.matrix[11], node.matrix[12], node.matrix[13],
                              node.matrix[14], node.matrix[15]);
  }
  const auto T = node.translation.empty()
                     ? parentMatrix
                     : glm::translate(parentMatrix,
                           glm::vec3(node.translation[0], node.translation[1],
                               node.translation[2]));
  const auto rotationQuat =
      node.rotation.empty()
          ? glm::quat(1, 0, 0, 0)
          : glm::quat(float(node.rotation[3]), float(node.rotation[0]),
                float(node.rotation[1]),
                float(node.rotation[2])); // prototype is w, x, y, z
  const auto TR = T * glm::mat4_cast(rotationQuat);
  return node.scale.empty() ? TR
                            : glm::scale(TR, glm::vec3(node.scale[0],
                                                 node.scale[1], node.scale[2]));
};

void computeSceneBounds(
    const tinygltf::Model &model, glm::vec3 &bboxMin, glm::vec3 &bboxMax)
{
  // Compute scene bounding box
  // todo refactor with scene drawing
  // todo need a visitScene generic function that takes a accept() functor
  bboxMin = glm::vec3(std::numeric_limits<float>::max());
  bboxMax = glm::vec3(std::numeric_limits<float>::lowest());
  if (model.defaultScene >= 0) {
    const std::function<void(int, const glm::mat4 &)> updateBounds =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto &node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix =
              getLocalToWorldMatrix(node, parentMatrix);
          if (node.mesh >= 0) {
            const auto &mesh = model.meshes[node.mesh];
            for (size_t pIdx = 0; pIdx < mesh.primitives.size(); ++pIdx) {
              const auto &primitive = mesh.primitives[pIdx];
              const auto positionAttrIdxIt =
                  primitive.attributes.find("POSITION");
              if (positionAttrIdxIt == end(primitive.attributes)) {
                continue;
              }
              const auto &positionAccessor =
                  model.accessors[(*positionAttrIdxIt).second];
              if (positionAccessor.type != 3) {
                std::cerr << "Position accessor with type != VEC3, skipping"
                          << std::endl;
                continue;
              }
              const auto &positionBufferView =
                  model.bufferViews[positionAccessor.bufferView];
              const auto byteOffset =
                  positionAccessor.byteOffset + positionBufferView.byteOffset;
              const auto &positionBuffer =
                  model.buffers[positionBufferView.buffer];
              const auto positionByteStride =
                  positionBufferView.byteStride ? positionBufferView.byteStride
                                                : 3 * sizeof(float);

              if (primitive.indices >= 0) {
                const auto &indexAccessor = model.accessors[primitive.indices];
                const auto &indexBufferView =
                    model.bufferViews[indexAccessor.bufferView];
                const auto indexByteOffset =
                    indexAccessor.byteOffset + indexBufferView.byteOffset;
                const auto &indexBuffer = model.buffers[indexBufferView.buffer];
                auto indexByteStride = indexBufferView.byteStride;

                switch (indexAccessor.componentType) {
                default:
                  std::cerr
                      << "Primitive index accessor with bad componentType "
                      << indexAccessor.componentType << ", skipping it."
                      << std::endl;
                  continue;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                  indexByteStride =
                      indexByteStride ? indexByteStride : sizeof(uint8_t);
                  break;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                  indexByteStride =
                      indexByteStride ? indexByteStride : sizeof(uint16_t);
                  break;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                  indexByteStride =
                      indexByteStride ? indexByteStride : sizeof(uint32_t);
                  break;
                }

                for (size_t i = 0; i < indexAccessor.count; ++i) {
                  uint32_t index = 0;
                  switch (indexAccessor.componentType) {
                  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                    index = *((const uint8_t *)&indexBuffer
                                  .data[indexByteOffset + indexByteStride * i]);
                    break;
                  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                    index = *((const uint16_t *)&indexBuffer
                                  .data[indexByteOffset + indexByteStride * i]);
                    break;
                  case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                    index = *((const uint32_t *)&indexBuffer
                                  .data[indexByteOffset + indexByteStride * i]);
                    break;
                  }
                  const auto &localPosition =
                      *((const glm::vec3 *)&positionBuffer
                              .data[byteOffset + positionByteStride * index]);
                  const auto worldPosition =
                      glm::vec3(modelMatrix * glm::vec4(localPosition, 1.f));
                  bboxMin = glm::min(bboxMin, worldPosition);
                  bboxMax = glm::max(bboxMax, worldPosition);
                }
              } else {
                for (size_t i = 0; i < positionAccessor.count; ++i) {
                  const auto &localPosition =
                      *((const glm::vec3 *)&positionBuffer
                              .data[byteOffset + positionByteStride * i]);
                  const auto worldPosition =
                      glm::vec3(modelMatrix * glm::vec4(localPosition, 1.f));
                  bboxMin = glm::min(bboxMin, worldPosition);
                  bboxMax = glm::max(bboxMax, worldPosition);
                }
              }
            }
          }
          for (const auto childNodeIdx : node.children) {
            updateBounds(childNodeIdx, modelMatrix);
          }
        };
    for (const auto nodeIdx : model.scenes[model.defaultScene].nodes) {
      updateBounds(nodeIdx, glm::mat4(1));
    }
  }
}



std::vector<glm::vec3> computeTangents(const tinygltf::Model &model, const tinygltf::Primitive &primitive)
{
  std::vector<glm::vec3> tangents;
 
  const auto positionAttrIdxIt = primitive.attributes.find("POSITION");
  if (positionAttrIdxIt == end(primitive.attributes)) {
    return tangents;
  }
  const auto texAttrIdxIt = primitive.attributes.find("TEXCOORD_0");
  if (texAttrIdxIt == end(primitive.attributes)) {
    return tangents;
  }

  const auto &positionAccessor = model.accessors[(*positionAttrIdxIt).second];
  if (positionAccessor.type != 3) {
    std::cerr << "Position accessor with type != VEC3, skipping" << std::endl;
    return tangents;
  }
  const auto &texAccessor = model.accessors[(*texAttrIdxIt).second];
  if (texAccessor.type != 2) {
    std::cerr << "Texture accessor with type != VEC2, skipping" << std::endl;
    return tangents;
  }

  const auto &positionBufferView = model.bufferViews[positionAccessor.bufferView];
  const auto positionByteOffset = positionAccessor.byteOffset + positionBufferView.byteOffset;
  const auto &positionBuffer = model.buffers[positionBufferView.buffer];
  const auto positionByteStride = positionBufferView.byteStride ? positionBufferView.byteStride : 3 * sizeof(float);

  const auto &texBufferView = model.bufferViews[texAccessor.bufferView];
  const auto &texBuffer = model.buffers[texBufferView.buffer];
  const auto texByteOffset = texAccessor.byteOffset + texBufferView.byteOffset;
  const auto texByteStride = texBufferView.byteStride ? texBufferView.byteStride : 2 * sizeof(float);

  if (primitive.indices >= 0) {
    const auto &indexAccessor   = model.accessors[primitive.indices];
    const auto &indexBufferView = model.bufferViews[indexAccessor.bufferView];
    const auto indexByteOffset  = indexAccessor.byteOffset + indexBufferView.byteOffset;
    const auto &indexBuffer     = model.buffers[indexBufferView.buffer];
    auto indexByteStride        = indexBufferView.byteStride;

    switch (indexAccessor.componentType) {
    default:
      std::cerr << "Primitive index accessor with bad componentType "
                << indexAccessor.componentType << ", skipping it." << std::endl; 
      return tangents;
    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
      indexByteStride = indexByteStride ? indexByteStride : sizeof(uint8_t); break;
    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
      indexByteStride = indexByteStride ? indexByteStride : sizeof(uint16_t); break;
    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
      indexByteStride = indexByteStride ? indexByteStride : sizeof(uint32_t); break;
    }

    glm::vec3 pos1; glm::vec3 pos2; glm::vec3 pos3;
    glm::vec2 uv1;  glm::vec2 uv2;  glm::vec2 uv3;

    for (size_t i = 0; i < indexAccessor.count; i+=3) { // compute tangents from indexes
      //std::cout << "indexed" << std::endl;
      uint32_t index1 = 0;
      uint32_t index2 = 0;
      uint32_t index3 = 0;
      switch (indexAccessor.componentType) {
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
          index1 = *((const uint8_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
          index2 = *((const uint8_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+1)]);
          index3 = *((const uint8_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+2)]); break;
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
          index1 = *((const uint16_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]); 
          index2 = *((const uint16_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+1)]);
          index3 = *((const uint16_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+2)]); break;
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
          index1 = *((const uint32_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]); 
          index2 = *((const uint32_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+1)]); 
          index3 = *((const uint32_t *)&indexBuffer.data[indexByteOffset + indexByteStride * (i+2)]); break;
      }

      glm::vec3 pos1 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * index1]);
      glm::vec3 pos2 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * index2]);
      glm::vec3 pos3 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * index3]);

      glm::vec2 uv1 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * index1]);
      glm::vec2 uv2 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * index2]);
      glm::vec2 uv3 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * index3]);

      const auto& tangent = getTangent(pos1, pos2, pos3, uv1 , uv2 , uv3);
      tangents.emplace_back(tangent);
      tangents.emplace_back(tangent);
      tangents.emplace_back(tangent);
    }
  } else { // compute tangents from next triangle
    for (size_t i = 0; i < positionAccessor.count; i+=3)
    {
      //std::cout << "listed" << std::endl;
      glm::vec3 pos1 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * i  ]);
      glm::vec3 pos2 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * i+1]);
      glm::vec3 pos3 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * i+2]);

      glm::vec2 uv1 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * i  ]);
      glm::vec2 uv2 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * i+1]);
      glm::vec2 uv3 = *((const glm::vec2 *)&texBuffer.data[texByteOffset + texByteStride * i+2]);

      const auto& tangent = getTangent(pos1, pos2, pos3, uv1 , uv2 , uv3);
      tangents.emplace_back(tangent);
      tangents.emplace_back(tangent);
      tangents.emplace_back(tangent);
    }
  }

  return tangents;
}


glm::vec3 getTangent(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec3& pos3, 
                     const glm::vec2& uv1 , const glm::vec2& uv2 , const glm::vec2& uv3)
{
  glm::vec3 edge1 = pos2 - pos1;
  glm::vec3 edge2 = pos3 - pos1;
  glm::vec2 deltaUV1 = uv2 - uv1;
  glm::vec2 deltaUV2 = uv3 - uv1;
  float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

  glm::vec3 tangent;
  tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
  tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
  tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
  return tangent;
}