#pragma once

#include <glm/gtc/type_ptr.hpp>




struct DirectionalLight
{
    DirectionalLight()
        : DirectionalLight(glm::vec3(0., 0., 1.), glm::vec3(1., 1., 1.))
    { }

    DirectionalLight(const glm::vec3& _direction, const glm::vec3& _intensity)
        : direction { glm::vec4(_direction, 0.) }, intensity { glm::vec4(_intensity, 0.) }
    { }
    
    glm::vec4 direction;
    glm::vec4 intensity;
};


struct PointLight
{
    PointLight()
        : PointLight(glm::vec3(0., 1., 0.), glm::vec3(1., 1., 1.), 1., 1.09, 0.032)
    { }

    PointLight(const glm::vec3& _position)
        : PointLight(_position, glm::vec3(1., 1., 1.), 1., 1.09, 0.032)
    { }

    PointLight(const glm::vec3& _position, const glm::vec3& _intensity, 
               const float _constant, const float _linear, const float _quadratic)
        : position { glm::vec4(_position, 1.) }, intensity { glm::vec4(_intensity, 0.) },
          constant { _constant }, linear { _linear }, quadratic { _quadratic }
    { }
    
    glm::vec4 position;
    glm::vec4 intensity;

    float constant;
    float linear; 
    float quadratic;
    float __pad0__;
};


struct SpotLight {
    SpotLight()
        : SpotLight(glm::vec3(0., 10., 0.), glm::vec3(-1.,0., 0.))
    { }

    SpotLight(const glm::vec3& _position, const glm::vec3& _direction)
        : SpotLight(_position, _direction, glm::vec3(1., 1., 1.), 12.5, 17.5)
    { }

    SpotLight(const glm::vec3& _position, const glm::vec3& _direction, const glm::vec3& _intensity, 
              const float _cutOff, const float _outerCutOff)
        : position { glm::vec4(_position, 1.) }, direction { glm::vec4(_direction, 0.) }
        , intensity { glm::vec4(_intensity, 0.) }, cutOff { _cutOff }, outerCutOff { _outerCutOff }
    { }


    glm::vec4 position;
    glm::vec4 direction;
    glm::vec4 intensity;
    float cutOff;
    float outerCutOff;
    float __pad0__;
    float __pad1__;
};
